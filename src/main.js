import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'

import ApiService from './services/api.service'
import {TokenService} from "./services/storage.service";

import VeeValidate, {Validator} from 'vee-validate'
import ru from 'vee-validate/dist/locale/ru'
import attributes from './attributes'

import Notifications from 'vue-notification'
Vue.use(Notifications)

Validator.localize('ru', Object.assign(ru, attributes))
Vue.use(VeeValidate, {locale: ru})

Vue.config.productionTip = false

// Set the base URL of the API
ApiService.init(process.env.VUE_APP_ROOT_API)

// If token exists set header
if (TokenService.getToken()) {
  ApiService.setHeader()
  ApiService.mount401Interceptor()
}

Vue.prototype.$api = ApiService

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
