import attributes from '../attributes'

export default {
  data() {
    return {
      attributeLabels: attributes.ru.attributes
    }
  }
}
