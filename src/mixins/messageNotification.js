export default {
  data() {
    return {
      routeName: ''
    }
  },

  methods: {
    notify(message) {
      let {text, from} = JSON.parse(message)

      if (this.doesShow(from)) {
        this.showMessageNotification(text, from)
      }
    },

    showMessageNotification(text, from) {

      let username = this.getUsernameById(from).toUpperCase()
      let title = '<b>' + username + '</b>'

      this.$notify({
        group: 'messages',
        title: title,
        text: text,
        duration: -1,
        data: {from: from}
      })
    },

    doesShow(from) {
      let routeName = this.$route.name
      let routes = ['home', 'chatList']

      if (routes.indexOf(routeName) !== -1) {
        return true
      }

      if (routeName === 'chat') {
        return from !== this.$route.params.chatId
      }

      return false
    }

  }
}
