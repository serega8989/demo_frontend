import {UserService, AuthenticationError} from '../services/user.service'
import {TokenService} from '../services/storage.service'
import router from '../router'
import store from '../store'


const state = {
  authenticating: false,
  username: TokenService.getUsername(),
  accessToken: TokenService.getToken(),
  authenticationErrorCode: 0,
  authenticationError: '',
  refreshTokenPromise: null
}

const getters = {
  loggedIn: (state) => {
    return state.accessToken ? true : false
  },

  authenticationErrorCode: (state) => {
    return state.authenticationErrorCode
  },

  authenticationError: (state) => {
    return state.authenticationError
  },

  authenticating: (state) => {
    return state.authenticating
  },

  username: (state) => {
    return state.username
  }
}

const actions = {
  async login({commit}, {username, password}) {
    commit('loginRequest');

    try {
      const userInfo = await UserService.login(username, password);
      commit('loginSuccess', userInfo)

      store.dispatch('chat/start')

      // Redirect the user to the page he first tried to visit or to the home view
      router.push(router.history.current.query.redirect || '/');

      return true
    } catch (e) {
      if (e instanceof AuthenticationError) {
        commit('loginError', {errorCode: e.errorCode, errorMessage: e.message})
      }

      return false
    }
  },

  logout({commit}) {
    UserService.logout()
    commit('logoutSuccess')
    router.push('/login')
  }
}

const mutations = {
  loginRequest(state) {
    state.authenticating = true;
    state.authenticationError = ''
    state.authenticationErrorCode = 0
  },

  loginSuccess(state, userInfo) {
    state.accessToken = userInfo.access_token
    state.username = userInfo.username
    state.authenticating = false;
  },

  loginError(state, {errorCode, errorMessage}) {
    state.authenticating = false
    state.authenticationErrorCode = errorCode
    state.authenticationError = errorMessage
  },

  logoutSuccess(state) {
    state.accessToken = ''
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
