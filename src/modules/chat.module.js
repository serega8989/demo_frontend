import ChatService from '../services/chat.service'
import store from '../store'

const state = {
  users: [],
  messages: [],
  queue: null,
  connected: false
}

const getters = {

  chatUsers: (state) => {
    return state.users
  },

  getMessagesById: state => id => {
    return state.messages[id]
  },

  getUsernameById: (state, getters) => id => {

    let username = ''
    getters.chatUsers.forEach((user) => {
      if (user.id == id) {
        username = user.username
      }
    })
    return username
  },

  sameOwner: state => (id, key) => {
    if (key === 0) {
      return false
    }
    let currentMessage = state.messages[id][key]
    let previousMessage = state.messages[id][key-1]

    return currentMessage.income === previousMessage.income

  }
}

const actions = {
  async start() {
    await store.dispatch('chat/getChatUsers')
    await store.dispatch('chat/getQueue')
    ChatService.connectWs(state.queue)
  },

  success({commit}) {
    commit('success')
  },

  async getQueue({commit}) {
    try {
      let queue = await ChatService.getQueue()
      commit('saveQueue', queue)
    } catch (e) {
      throw (e)
    }
  },

  async sendMessage({commit}, {text, to}) {
    try {
      ChatService.sendMessage(text, to)
      commit('pushMessage', {text: text, userId: to})
    } catch (e) {
      throw (e)
    }
  },

  async getChatUsers({commit}) {
    try {
      if (!state.users.length) {
        const users = await ChatService.getChatUsers()
        commit('pullUsers', users)
        commit('initMessages')
      }
    } catch (e) {
      throw e
    }
  },

  handleMessage({commit}, message) {
    message = JSON.parse(message)
    if (message.income === undefined) {
      message.income = true
    }
    commit('pushMessage', {text: message.text, userId: message.from, income: message.income})
  },

  pushChat({commit}, message) {
    commit('pushMessage', message)
  }
}

const mutations = {
  pushMessage(state, message) {
    let userId = message.userId
    state.messages[userId].push(message)
  },

  pullUsers(state, users) {
    state.users = users
  },

  saveQueue(state, queue) {
    state.queue = queue
  },

  success(state) {
    state.connected = true
  },

  initMessages(state) {
    state.users.forEach((user) => {
      if (state.messages[user.id] === undefined) {
        state.messages[user.id] = []
      }
    })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
