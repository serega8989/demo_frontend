import axios from 'axios'
import {TokenService} from './storage.service'
import store from '../store'

const ApiService = {

  _401interceptor: null,

  _422interceptor: null,

  validator: null,

  init(baseURL) {
    axios.defaults.baseURL = baseURL
    axios.defaults.headers.common['Accept'] = 'application/json'
    axios.defaults.headers.common['Content-Type'] = 'application/json'
  },

  setHeader() {
    axios.defaults.headers.common['X-api-key'] = TokenService.getToken()
  },

  removeHeader() {
    axios.defaults.headers.common = {}
  },

  get(resource) {
    return axios.get(resource)
  },

  post(resource, data) {
    return axios.post(resource, data)
  },

  put(resource, data) {
    return axios.put(resource, data)
  },

  delete(resource) {
    return axios.delete(resource)
  },

  customRequest(data) {
    return axios(data)
  },

  setValidator(validator) {
    this.validator = validator
    this.mount422Interceptor()
  },

  deleteValidator() {
    this.validator = null
    this.unmount422Interceptor()
  },

  mount422Interceptor() {
    this._422interceptor = axios.interceptors.response.use(
      (response) => {
        return response
      },
      (error) => {
        if (error.response.status === 422 && this.validator !== null) {
          error.response.data.forEach((element) => {
            this.validator.errors.add({
              field: element.field,
              msg: element.message
            })
          })
        }
        return Promise.reject(error)
      }
    )
  },

  unmount422Interceptor() {
    // Eject the interceptor
    axios.interceptors.response.eject(this._422interceptor)
  },

  mount401Interceptor() {
    this._401interceptor = axios.interceptors.response.use(
      (response) => {
        return response
      },
      async (error) => {
        if (error.request.status == 401) {
          store.dispatch('auth/logout')
          throw error
        }
        // If error was not 401 just reject as is
        throw error
      }
    )
  },

  unmount401Interceptor() {
    // Eject the interceptor
    axios.interceptors.response.eject(this._401interceptor)
  }

}

export default ApiService
