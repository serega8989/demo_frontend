import ApiService from './api.service'
import store from '../store'
import Stomp from 'stompjs'
import { bus } from '../bus'
import { mapActions } from "vuex";

const ChatService = {

  socketUrl: process.env.VUE_APP_SOCKET_URL,
  socket: null,
  stompClient: null,
  queue: null,

  ...mapActions('chat', ['handleMessage']),

  sendMessage: async function (text, userId) {
    try {
      await ApiService.post(
        '/chat/send-message',
        {
          text: text,
          userId: userId
        }
      )

      store.dispatch('chat/pushChat', {
        text: text,
        userId: userId,
        income: false
      })
      return true;
    } catch (e) {
      throw e
    }
  },

  getChatUsers: async function () {
    try {
      const response = await ApiService.get('/chat/users')
      return response.data
    } catch (e) {
      throw e
    }
  },

  getQueue: async function () {
    try {
      const response = await ApiService.get('/chat/get-queue')
      return response.data.queue
    } catch (e) {
      throw e
    }
  },

  connectWs(queue) {
    this.socket = new WebSocket(this.socketUrl)
    this.stompClient = Stomp.over(this.socket)
    this.stompClient.debug = null
    this.stompClient.heartbeat.outgoing = 10000
    this.stompClient.heartbeat.incoming = 0
    this.stompClient.connect(
      process.env.VUE_APP_CHAT_USER,
      process.env.VUE_APP_CHAT_PASS,
      (frame) => {
        // store.dispatch('chat/success')
        this.stompClient.subscribe(
          '/amq/queue/' + queue,
          (message) => {
            store.dispatch('chat/handleMessage', message.body)
            bus.$emit('incomeMessage', message.body)
          }
        )
      },
      (e) => {
        console.log('reconnecting ws')
        setTimeout(() => {
          this.connectWs(queue)
        }, 3000)
      },
    )
  },

}

export default ChatService
