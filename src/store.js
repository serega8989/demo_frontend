import Vue from 'vue'
import Vuex from 'vuex'
import authModule from './modules/auth.module'
import chatModule from './modules/chat.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth: authModule,
    chat: chatModule
  }
})
